const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController")
const auth = require("../auth.js")

// route for creating a course
// router.post("/", (req,res)=> {
//     courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// })

// -----------ACTIVITY
router.post("/", auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    courseController.addCourse({isAdmin: userData.isAdmin}, req.body).then(result => res.send(result)).catch(error => res.send(error));

})

// route for getting all courses
router.get("/all", (req,res)=> {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

// route for getting all active courses
router.get("/", (req,res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})
// route for getting specific course
router.get("/:courseId", (req,res) => {
    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// route for updating courses
router.put("/:courseId", auth.verify, (req,res) => {
    courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// route for archiving courses
router.put("/:courseId/archive", auth.verify, (req,res) => {
    courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})




module.exports = router;