const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth.js")

// Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req,res)=>{
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

// route for user registration
router.post('/register', (req,res)=>{
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// route for user authentication
router.post("/login",(req,res)=>{
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// s37----------------ACTIVITY
// route for details
router.get("/details", auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController))
})

// route for user enrollment (ADMIN ONLY) ----------- ACTIVITY
router.post("/enroll", auth.verify, (req, res) => {
	let data ={
		userId : auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});









module.exports = router