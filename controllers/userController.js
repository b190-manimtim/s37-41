const User = require("../models/User")
const Course = require("../models/Course")
const auth = require("../auth")
const bcrypt = require("bcrypt")


module.exports.checkEmailExists = (reqBody) => {
    return User.find( { email: reqBody.email } ).then(result => {
        if(result.length > 0){
            return true
        } else {
            return false
        }
    })

}

// User Registration

module.exports.registerUser = (reqBody) => {
    let newUSer = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        // hashSync- bcrypt's method for encrypting the property the password of the user once they have successfully registered in our database
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    return newUSer.save().then((user,error)=>{
        if(error){
            return false
        } else{
            return true;
        }
    })
}

// user login
module.exports.loginUser = (reqBody) => {
    return User.findOne( { email: reqBody.email } ).then(result => {
        // if the user does not exist
        if(result === null){
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect){
                return{access: auth.createAccessToken(result)}
            } else {
                return false
            }
        }
    })
}

// getProfile ------------ ACTIVITY s38

module.exports.getProfile = (reqBody) => {
    return User.findOne( { id: reqBody.id } ).then(result => {
        
            result.password = ""
            return result
        
    })

}

// enroll a user to a class
/*
	1. to find the document in the database using the user's ID
	2.add the course ID to the user's enrollments array
	3. update the document in the MongoDB Atlas DB
*/
// async await will be used in enrolling since we have two documents to be updated in our database: user document and course document
module.exports.enroll = async (data) =>{
	// adding the courseId in the enrollments array of the user
	// returns boolean depending if the updating of the document is successful (true) or failed (false)
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});

		// saves the updated user information in the database
		return user.save().then((user, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
	/*
		try to update the enrollees array in the course documents using the codes above as your guide
		10 minutes: 6:03 pm (do not create any Postman requests yet)
	*/
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// adding of userId in the enrollees array
		course.enrollees.push({userId: data.userId});

		// saves the updated course information in the database
		return course.save().then((course, error)=>{
			if (error) {
				return false;
			}else{
				return true;
			}
		})
	});

	// condition that will check if the user and course documents have been updated
	if (isUserUpdated && isCourseUpdated) {
		// User enrollment successful
		return true;
	}else{
		//User enrollement failed
		return false; 
	}

}
