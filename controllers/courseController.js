const Course = require("../models/Course")
const auth = require("../auth.js")
// create new course
// Mini activity
module.exports.addCourse = (userData, courseData) => 
{
    if (userData.isAdmin) 
    {
        let newCourse = new Course({
            name: courseData.name,
            description: courseData.description,
            price: courseData.price
        });
        return newCourse.save().then((course, error) =>
        {
            if (error) 
            {
                console.log(error)
                return false;
            } 
            else 
            {
                return course;
            }
        })
    } 
    else 
    {
        return Promise.reject('Unauthorised user');
    }
}

// course for getting all course -- mini activity
module.exports.getAllCourses = () => {
    return Course.find( {} ).then(result => {
        return result
})
}

// course for getting all active course -- mini activity
module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result =>{
        return result
    })
}
    

// route for retreiving a specific course
module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then((result, error)=>{
        if(error){
            return false
        } else{
            return result
        }
    })
    }

// route for updating a course
module.exports.updateCourse = (reqParams, reqBody) => {
    let updateCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, err)=>{
        if(err){
            return false
        } else {
            return true
        }
    })
}

// route for archive a course
module.exports.updateCourse = (reqParams, reqBody) => {
    let updateCourse = {
        isActive: false
    }
    return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, err)=>{
        if(err){
            return false
        } else {
            return true
        }
    })
}